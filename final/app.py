"""
A Covid-19 dashboard application that searches for airports based on user input.
"""
import flask
from flask.views import MethodView
from index import Index
from name import Name
from date import Date

app = flask.Flask(__name__)       # our Flask app

"""
Landing page for the application
"""
app.add_url_rule('/',
                 view_func=Index.as_view('index'),
                 methods=["GET"])

"""
/name redirects to search covid-19 data by country name
"""
app.add_url_rule('/name',
                 view_func=Name.as_view('name'),
                 methods=['GET', 'POST'])

"""
/date redirects to search daily covid-19 report by country name
"""
app.add_url_rule('/date',
                 view_func=Date.as_view('date'),
                 methods=['GET', 'POST'])


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8001, debug=True)
