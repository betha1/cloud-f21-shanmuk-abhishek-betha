"""
Python list model
"""
from Model import Model

class model(Model):
    def __init__(self):
        self.name = ""
        self.name2 = ""
        self.date = ""

    def returnName(self):
        """
        Returns country name entered by the user
        :return: airport name
        """
        return self.name

    def returnName2(self):
        """
        Returns country name entered by the user
        :return: Iata/Icao
        """
        return self.name2
    
    def returnDate(self):
        """
        Returns date entered by the user
        :return: Iata/Icao
        """
        return self.date

    def insertName(self, name):
        """
        Stores country name
        :return: True
        """
        self.name = name
        return True

    def insertName2(self, name2):
        """
        Stores country name
        :return: True
        """
        self.name2 = name2
        return True
    
    def insertDate(self, date):
        """
        Stores date
        :return: True
        """
        self.date = date
        return True
