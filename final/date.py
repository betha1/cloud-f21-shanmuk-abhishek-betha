from flask import redirect, request, url_for, render_template
from flask.views import MethodView
from model_pylist import model
import requests
import json

model = model()


class Date(MethodView):
    def get(self):
        """
        Returns the page when requested
        """
        url = "https://covid-19-data.p.rapidapi.com/report/country/name"

        querystring = {"name": model.name2, "date": model.date}
        # querystring = {"name":"Italy","date":"2020-04-01"}

        headers = {
            'x-rapidapi-host': "covid-19-data.p.rapidapi.com",
            'x-rapidapi-key': "0ee2f7d58amsh2683689776c3476p1c2f93jsn48c62745e1a8"
        }

        response = requests.request("GET", url, headers=headers, params=querystring)
        print(response.text)

        print('input is : ', model.date)
        res = json.loads(response.text)
        entries = []
        if (len(res) > 0 and 'type' not in res):
            '''arr = res['items']'''
            entries = [
                dict(country=res[0]['country'], province=res[0]['provinces'][0]['province'],
                     confirmed=res[0]['provinces'][0]['confirmed'], recovered=res[0]['provinces'][0]['recovered'],
                     deaths=res[0]['provinces'][0]['deaths'], active=res[0]['provinces'][0]['active'],
                     latitude=res[0]['latitude'], longitude=res[0]['longitude'], date=res[0]['date'])
            ]
        return render_template('date.html', entries=entries)

    def post(self):
        """
        Accepts POST requests, and processes the form;
        Redirect to index when completed.
        """

        model.insertName2(request.form['name'])
        model.insertDate(request.form['date'])
        return redirect(url_for('date'))