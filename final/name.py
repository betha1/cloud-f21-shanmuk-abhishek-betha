from flask import redirect, request, url_for, render_template
from flask.views import MethodView
from model_pylist import model
import requests
import json

model = model()

class Name(MethodView):
    def get(self):
        """
        Returns the page when requested
        """
        url = "https://covid-19-data.p.rapidapi.com/country"

        querystring = {"name": model.name}

        headers = {
            'x-rapidapi-host': "covid-19-data.p.rapidapi.com",
            'x-rapidapi-key': "0ee2f7d58amsh2683689776c3476p1c2f93jsn48c62745e1a8"
        }

        response = requests.request("GET", url, headers=headers, params=querystring)

        entries = []
        print("input from form", model.name)
        print( "response ", response.text)
        if(len(response.text) > 0) :
            res = json.loads(response.text)
            if(len(res) > 0):
                print(res[0]['country'])
                entries = [
                    dict(country=res[0]['country'], code=res[0]['code'], confirmed=res[0]['confirmed'], recovered=res[0]['recovered'], critical=res[0]['critical'], deaths=res[0]['deaths'], latitude=res[0]['latitude'], longitude=res[0]['longitude'], lastChange=res[0]['lastChange'], lastUpdate=res[0]['lastUpdate'])
                    ]
        return render_template('name.html', entries=entries)

    def post(self):
        """
        Accepts POST requests, and processes the form;
        Redirect to name when completed.
        """
        model.insertName(request.form['name'])
        return redirect(url_for('name'))
