"""
A simple courses flask app.
"""
import flask
from flask.views import MethodView
from index import Index
from add import Add
from courses import Courses

app = flask.Flask(__name__)       # our Flask app

"""
Landing page for the application
"""
app.add_url_rule('/',
                 view_func=Index.as_view('index'),
                 methods=["GET"])

"""
/add redirects to Add course
"""
app.add_url_rule('/add',
                 view_func=Add.as_view('add'),
                 methods=['GET', 'POST'])

"""
/courses will display all the courses entered
"""
app.add_url_rule('/courses',
                 view_func=Courses.as_view('courses'),
                 methods=["GET"])
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000, debug=True)
