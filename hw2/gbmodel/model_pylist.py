"""
Python list model
"""
from .Model import Model

class model(Model):
    def __init__(self):
        self.courseentries = []

    def select(self):
        """
        Returns courseentries list of lists
        Each list in courseentries contains: department, coursenumber, quarter, year, instructor, review
        :return: List of lists
        """
        return self.courseentries

    def insert(self, department, coursenumber, quarter, year, instructor, review):
        """
        Appends a new list of values representing new course into courseentries
        :param department: String
        :param coursenumber: String
        :param quarter: String
        :param year: Integer
        :param instructor: String
        :param review: String
        :return: True
        """
        params = [department, coursenumber, quarter, year, instructor, review]
        self.courseentries.append(params)
        return True
