from flask import render_template
from flask.views import MethodView
import gbmodel

class Courses(MethodView):
    def get(self):
        """
        Gets all the rows from the courses table
        maps it to the courses variable
        renders and display the index page
        """
        model = gbmodel.get_model()
        courses = [dict(department=row[0], coursenumber=row[1], quarter=row[2], year=row[3], instructor=row[4], review=row[5] ) for row in model.select()]
        return render_template('courses.html',courses=courses)
